package ro.unitbv.studnet.api.domain;

import java.util.List;

public class User {
    protected String id;
    protected String name;
    protected String email;
    protected String role;
    protected List<Topic> topics;

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, String email, String role) {
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public User(String id, String name, String email, String role, List<Topic> topics) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
        this.topics = topics;
    }

    public User(String name, String email, String role, List<Topic> topics) {
        this.name = name;
        this.email = email;
        this.role = role;
        this.topics = topics;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
