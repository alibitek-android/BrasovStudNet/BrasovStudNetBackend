package ro.unitbv.studnet.api.domain;

public class LoginResponse implements ResponseObject {

    private boolean authenticated;
    private User user;

    LoginResponse(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public LoginResponse(boolean authenticated, User user) {
        this.authenticated = authenticated;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
