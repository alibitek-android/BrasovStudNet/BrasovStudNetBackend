package ro.unitbv.studnet.api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;


@Configuration
@EnableWebSecurity(debug = false)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Autowired
    private CustomBasicAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void configure(WebSecurity web) throws Exception {
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Don't generate sessions
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Intercept all requests matching the specified Ant matcher and make sure that they come from an authenticated user
        http.authorizeRequests().antMatchers("/api/**").hasAnyAuthority(new String[]{"student", "teacher"}).anyRequest().authenticated();

        // Disable CSRF (Cross Site Request Forgery)
        http.csrf().disable();

        // What happens when an unauthenticated client tries to access a restricted resource
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);

        // Request the client to submit a user’s login credentials as a Basic HTTP Authentication
        http.httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(new PasswordEncoder() {
                    @Override
                    public String encode(CharSequence rawPassword) {
                        return null;
                    }

                    @Override
                    public boolean matches(CharSequence rawPassword, String encodedPassword) {

                        // TODO: Return 401 Unauthorized
                        if (rawPassword == null || encodedPassword == null)
                            return false;

                            String sql = "SELECT crypt(?, ?) as password_hash";

                            String hashedPassword = jdbcTemplate.query(sql, new String[]{rawPassword.toString(), encodedPassword}, new ResultSetExtractor<String>() {
                                @Override
                                public String extractData(ResultSet rs) throws SQLException, DataAccessException {
                                    rs.next();
                                    return rs.getString("password_hash");
                                }
                            });

                            //logger.debug("rawPassword: {} | encodedPassword: {} | hashedPassword: {}", rawPassword, encodedPassword, hashedPassword);

                            return encodedPassword.equals(hashedPassword);
                        }
                    }

                    ).usersByUsernameQuery(
                            "select username, password, enabled from api.get_user_credentials(?)")
                    .authoritiesByUsernameQuery(
                            "select username, role from api.get_user_roles(?)");
    }
}
