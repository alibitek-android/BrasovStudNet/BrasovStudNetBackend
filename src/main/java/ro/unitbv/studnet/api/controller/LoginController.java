package ro.unitbv.studnet.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.unitbv.studnet.api.domain.*;

import java.util.Objects;

@RestController
@RequestMapping("/api/1")
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> login(@RequestParam(value="username") String username,
                             @RequestParam(value="password") String password) {

        if (Objects.equals(username, "Alexandru.Butum@student.unitbv.ro") && Objects.equals(password, "test")) {

            return new ResponseEntity<>(new LoginResponse(true, new User(username, username, Role.STUDENT.toString(), null)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), "The provided credentials are invalid"), HttpStatus.UNAUTHORIZED);
        }
    }
}
