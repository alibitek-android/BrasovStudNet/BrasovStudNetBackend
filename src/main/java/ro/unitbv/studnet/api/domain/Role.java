package ro.unitbv.studnet.api.domain;

public enum Role {
    STUDENT,
    TEACHER,
    SYSADMIN,
    CAMPUS_ADMIN;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
