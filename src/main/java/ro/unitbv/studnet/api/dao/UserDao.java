package ro.unitbv.studnet.api.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import ro.unitbv.studnet.api.domain.*;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class UserDao extends JdbcDaoSupport implements IUserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    private static final String STUDENT_DETAILS_QUERY = "\n" +
            "select e.student_name, \n" +
            "\temail_hash as id, \n" +
            "\temail, \n" +
            "\tfacultatea, \n" +
            "\tciclul_de_invatamant, \n" +
            "\tforma_de_invatamant, \n" +
            "\tdomeniul, \n" +
            "\tan_de_studiu, \n" +
            "\tprogramul_de_studii, \n" +
            "\tgrupa, \n" +
            "\t(select campus_number from student_accomodations a where a.email = e.email) as campus_number, \n" +
            "\t(select room_number from student_accomodations a where a.email = e.email) as room_number \n" +
            "from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id \n" +
            "where email = lower(?) and r.name = api.get_user_type_name(email)";

    private static final String USER_BY_ID_QUERY = "\n" +
            "select e.student_name, \n" +
            "\temail_hash as id, \n" +
            "\temail, \n" +
            "\tfacultatea, \n" +
            "\tciclul_de_invatamant, \n" +
            "\tforma_de_invatamant, \n" +
            "\tdomeniul, \n" +
            "\tan_de_studiu, \n" +
            "\tprogramul_de_studii, \n" +
            "\tgrupa, \n" +
            "\t(select campus_number from student_accomodations a where a.email = e.email) as campus_number, \n" +
            "\t(select room_number from student_accomodations a where a.email = e.email) as room_number \n" +
            "from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id \n" +
            "where email_hash = lower(?) and r.name = api.get_user_type_name(email)";

    private static final String TEACHER_DETAILS_QUERY = "";

    private static final String TOPICS_QUERY = "select t.name, t.address, (select type from topic_types where id = t.type_id) from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id \n" +
            "join users_topics ut on ut.user_id = u.id join topics t on t.id = ut.topic_id\n" +
            "where email = lower(?) and r.name = ?";
    private static final String CAMPUS_QUERY = "select * from campuses where id = ?";

    private static final String ALL_STUDENTS_QUERY = "\n" +
            "select e.student_name, \n" +
            "\temail_hash as id, \n" +
            "\temail, \n" +
            "\tfacultatea, \n" +
            "\tciclul_de_invatamant, \n" +
            "\tforma_de_invatamant, \n" +
            "\tdomeniul, \n" +
            "\tan_de_studiu, \n" +
            "\tprogramul_de_studii, \n" +
            "\tgrupa, \n" +
            "\t(select campus_number from student_accomodations a where a.email = e.email) as campus_number, \n" +
            "\t(select room_number from student_accomodations a where a.email = e.email) as room_number \n" +
            "from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id where r.name = 'student'\n";

    private static final String STUDENTS_FOR_TOPIC_QUERY = "\n" +
            "select e.student_name, \n" +
            "\temail_hash as id, \n" +
            "\temail, \n" +
            "\tfacultatea, \n" +
            "\tciclul_de_invatamant, \n" +
            "\tforma_de_invatamant, \n" +
            "\tdomeniul, \n" +
            "\tan_de_studiu, \n" +
            "\tprogramul_de_studii, \n" +
            "\tgrupa, \n" +
            "\t(select campus_number from student_accomodations a where a.email = e.email) as campus_number, \n" +
            "\t(select room_number from student_accomodations a where a.email = e.email) as room_number \n" +
            "from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id\n" +
            "join users_topics ut on ut.user_id = u.id join topics t on ut.topic_id = t.id where r.name = 'student' and t.address = ?" +
            "order by campus_number, room_number";

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    protected void init() {
        setDataSource(dataSource);
    }

    private final class StudentMapper implements RowMapper<Student> {

        @Override
        public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
            Student student = new Student(rs.getString("id"), rs.getString("student_name"), rs.getString("email"), Role.STUDENT.name());
            Enrollment enrollment = new Enrollment(
                    rs.getString("facultatea"),
                    rs.getString("ciclul_de_invatamant"),
                    rs.getString("forma_de_invatamant"),
                    rs.getString("domeniul"),
                    rs.getString("an_de_studiu"),
                    rs.getString("programul_de_studii"),
                    rs.getInt("grupa")
            );

            student.setEnrollment(enrollment);
            Integer campusNumber = rs.getInt("campus_number");

            Accomodation accomodation = UserDao.this.getJdbcTemplate().query(CAMPUS_QUERY, new PreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    ps.setInt(1, campusNumber );
                }
            }, new ResultSetExtractor<Accomodation>() {
                @Override
                public Accomodation extractData(ResultSet rs) throws SQLException, DataAccessException {

                    if (rs.next()) {

                        Accomodation acc = new Accomodation();

                        acc.setName(rs.getString("nume"));
                        acc.setAddress(rs.getString("address"));
                        acc.setConstructionYear(rs.getInt("construction_year"));
                        acc.setAreaSqm(rs.getInt("area_square_meters"));
                        acc.setNumberOfPlaces(rs.getInt("number_of_places"));
                        acc.setAdministrator(rs.getString("administrator"));
                        acc.setComitteChairman(rs.getString("committee_chairman"));
                        acc.setPhoneNumber(rs.getString("phone_number"));
                        acc.setComplex(rs.getString("complex"));

                        return acc;
                    }

                    return null;
                }
            });

            if (accomodation != null) {
                accomodation.setCampusNumber(rs.getInt("campus_number"));
                accomodation.setRoomNumber(rs.getInt("room_number"));
            }

            List<Topic> topics = getJdbcTemplate().query(TOPICS_QUERY, new Object[] { rs.getString("email"), "student" }, new RowMapper<Topic>() {

                public Topic mapRow(ResultSet rs, int rowNum) throws SQLException {

                    return new Topic(rs.getString("name"), rs.getString("address"), rs.getString("type"));
                }

            });

            student.setTopics(topics);

            student.setAccomodation(accomodation);

            student.setLocation(new Location("Brasov"));

            return student;
        }
    }

    public User getUserDetails(String email) {

        logger.info("[UserDao::getUserDetails] Email: {}", email);
        // TODO: Query to determine user type and then use a different mapper for each type

        return getJdbcTemplate().queryForObject(STUDENT_DETAILS_QUERY, new Object[] { email }, new StudentMapper());
    }

    @Override
    public List<Student> getStudentsForTopic(String topicAddress) {
        return getJdbcTemplate().query(STUDENTS_FOR_TOPIC_QUERY, new Object[] {topicAddress}, new StudentMapper());
    }

    @Override
    public List<Student> getAllStudents() {
        return getJdbcTemplate().query(ALL_STUDENTS_QUERY, new StudentMapper());
    }

    @Override
    public User getUserById(String id) {
        logger.info("[UserDao::getUserById] Email: {}", id);
        return getJdbcTemplate().queryForObject(USER_BY_ID_QUERY, new Object[] { id }, new StudentMapper());
    }
}
