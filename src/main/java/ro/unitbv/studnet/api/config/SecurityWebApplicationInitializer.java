package ro.unitbv.studnet.api.config;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

// Automatically register the springSecurityFilterChain Filter for every URL in the application
// If Filters are added within other WebApplicationInitializer instances
// we can use @Order to control the ordering of the Filter instances.
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
