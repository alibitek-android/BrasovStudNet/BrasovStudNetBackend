package ro.unitbv.studnet.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.unitbv.studnet.api.dao.ITokenDao;
import ro.unitbv.studnet.api.domain.Token;

@Component
public class TokenService implements ITokenService {

    @Autowired
    ITokenDao tokenDao;

    @Override
    public boolean register(Token token, String email) {
        return tokenDao.registerToken(token, email);
    }
}
