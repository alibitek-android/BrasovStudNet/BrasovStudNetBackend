package ro.unitbv.studnet.api.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import ro.unitbv.studnet.api.domain.ErrorResponse;

@Component
public class CustomBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    private static Logger logger = LoggerFactory.getLogger(CustomBasicAuthenticationEntryPoint.class);

    CustomBasicAuthenticationEntryPoint() {
        setRealmName("Brasov StudNet");
    }

    ObjectMapper mapper = new ObjectMapper();

    /**
     * When authentication fails.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, ServletException {

        String path = request.getRequestURI().substring(request.getContextPath().length());
        logger.info(String.format("CustomBasicAuthenticationEntryPoint.commence() - %s - %s", path, authException.getMessage()));
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.addHeader("WWW-Authenticate", "Basic realm=\"" + getRealmName() + "\"");
        mapper.writeValue(response.getWriter(), new ErrorResponse(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage()).toString());
    }
}