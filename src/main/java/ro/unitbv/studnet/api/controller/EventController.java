package ro.unitbv.studnet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.unitbv.studnet.api.domain.Event;
import ro.unitbv.studnet.api.service.IEventService;

import java.util.List;

@RestController
@RequestMapping("/api/1/events")
public class EventController {

    @Autowired
    private IEventService eventService;

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    List<Event> getEvents() {
        return eventService.getEvents();
    }

    @RequestMapping(value = {"/year/{year}/month/{month}"}, method = RequestMethod.GET)
    List<Event> getEventsByYearMonth(@PathVariable int year, @PathVariable int month) {
        return eventService.getEventsByYearMonth(year, month);
    }
}
