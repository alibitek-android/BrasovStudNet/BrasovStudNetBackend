package ro.unitbv.studnet.api.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ro.unitbv.studnet.api.domain.ErrorResponse;

import java.security.Principal;

@Controller
@RequestMapping(value = "/error")
public class CustomErrorController implements ErrorController {

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ErrorResponse unauthorized(Principal user) {

        ErrorResponse error = new ErrorResponse();

        error.setStatus(HttpStatus.UNAUTHORIZED.value());
        error.setMessage("401 Unauthorized");

        if (user != null) {
            error.setMessage("Hi " + user.getName() + ", you do not have permission to access this page!");
        } else {
            error.setMessage("You do not have permission to access this page!");
        }

        return error;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
