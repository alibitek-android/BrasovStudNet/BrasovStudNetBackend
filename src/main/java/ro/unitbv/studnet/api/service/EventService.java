package ro.unitbv.studnet.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.unitbv.studnet.api.dao.IEventDao;
import ro.unitbv.studnet.api.dao.IUserDao;
import ro.unitbv.studnet.api.domain.Event;

import java.util.List;

@Component
public class EventService implements IEventService {
    @Autowired
    IEventDao eventDao;

    @Override
    public List<Event> getEvents() {
        return eventDao.getEvents();
    }

    @Override
    public List<Event> getEventsByYearMonth(int year, int month) {
        return eventDao.getEventsByYearMonth(year, month);
    }


}
