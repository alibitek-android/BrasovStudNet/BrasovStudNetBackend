package ro.unitbv.studnet.api.domain;

public class Enrollment {
    private String nume;
    private String cicluDeInvatamant;
    private String formaDeInvatamant;
    private String domeniul;
    private String anDeStudiu;
    private String programulDeStudii;
    private Integer grupa;

    public Enrollment() {
    }

    public Enrollment(String nume, String cicluDeInvatamant, String formaDeInvatamant, String domeniul, String anDeStudiu, String programulDeStudii, Integer grupa) {
        this.nume = nume;
        this.cicluDeInvatamant = cicluDeInvatamant;
        this.formaDeInvatamant = formaDeInvatamant;
        this.domeniul = domeniul;
        this.anDeStudiu = anDeStudiu;
        this.programulDeStudii = programulDeStudii;
        this.grupa = grupa;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getCicluDeInvatamant() {
        return cicluDeInvatamant;
    }

    public void setCicluDeInvatamant(String cicluDeInvatamant) {
        this.cicluDeInvatamant = cicluDeInvatamant;
    }

    public String getFormaDeInvatamant() {
        return formaDeInvatamant;
    }

    public void setFormaDeInvatamant(String formaDeInvatamant) {
        this.formaDeInvatamant = formaDeInvatamant;
    }

    public String getDomeniul() {
        return domeniul;
    }

    public void setDomeniul(String domeniul) {
        this.domeniul = domeniul;
    }

    public String getAnDeStudiu() {
        return anDeStudiu;
    }

    public void setAnDeStudiu(String anDeStudiu) {
        this.anDeStudiu = anDeStudiu;
    }

    public String getProgramulDeStudii() {
        return programulDeStudii;
    }

    public void setProgramulDeStudii(String programulDeStudii) {
        this.programulDeStudii = programulDeStudii;
    }

    public Integer getGrupa() {
        return grupa;
    }

    public void setGrupa(Integer grupa) {
        this.grupa = grupa;
    }
}
