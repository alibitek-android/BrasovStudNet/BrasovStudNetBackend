package ro.unitbv.studnet.api.domain;

import java.sql.Date;
import java.sql.Timestamp;

public class Event {

    String name;
    String description;
    Timestamp start_at;
    Timestamp end_at;

    public Event() {
    }

    public Event(String name, String description, Timestamp start_at, Timestamp end_at) {
        this.name = name;
        this.description = description;
        this.start_at = start_at;
        this.end_at = end_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getStart_at() {
        return start_at;
    }

    public void setStart_at(Timestamp start_at) {
        this.start_at = start_at;
    }

    public Timestamp getEnd_at() {
        return end_at;
    }

    public void setEnd_at(Timestamp end_at) {
        this.end_at = end_at;
    }
}
