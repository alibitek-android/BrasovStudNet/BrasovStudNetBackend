package ro.unitbv.studnet.api.service;

import ro.unitbv.studnet.api.domain.Token;

public interface ITokenService {
    boolean register(Token token, String email);
}
