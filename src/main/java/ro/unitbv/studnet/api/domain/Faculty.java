package ro.unitbv.studnet.api.domain;

import java.util.List;

public class Faculty {
    private String name;
    private List<Department> departments;
    private String address;
    private String email;
    private String secretariatPhone;

    public Faculty() {

    }

    public Faculty(String name) {
        this.name = name;
    }

    public Faculty(String name, String address, List<Department> departments, String email, String secretariatPhone) {
        this.address = address;
        this.departments = departments;
        this.email = email;
        this.name = name;
        this.secretariatPhone = secretariatPhone;
    }
}
