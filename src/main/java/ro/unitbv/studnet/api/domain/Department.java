package ro.unitbv.studnet.api.domain;

public class Department {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department(String name) {

        this.name = name;
    }

    public Department() {
    }
}
