package ro.unitbv.studnet.api.domain;

public class Accomodation {
    private String name;
    private Integer campusNumber;
    private Integer roomNumber;
    private String address;
    private Integer constructionYear;
    private Integer areaSqm;
    private Integer numberOfPlaces;
    private String administrator;
    private String comitteChairman;
    private String phoneNumber;
    private String complex;

    public String getComplex() {
        return complex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComplex(String complex) {
        this.complex = complex;
    }

    public Accomodation() {

    }

    public Accomodation(Integer campusNumber, Integer roomNumber) {
        this.campusNumber = campusNumber;
        this.roomNumber = roomNumber;
    }

    public Accomodation(Integer campusNumber, Integer roomNumber, String address, Integer constructionYear, Integer areaSqm, Integer numberOfPlaces, String administrator, String comitteChairman, String phoneNumber) {
        this.campusNumber = campusNumber;
        this.roomNumber = roomNumber;
        this.address = address;
        this.constructionYear = constructionYear;
        this.areaSqm = areaSqm;
        this.numberOfPlaces = numberOfPlaces;
        this.administrator = administrator;
        this.comitteChairman = comitteChairman;
        this.phoneNumber = phoneNumber;
    }

    public Integer getCampusNumber() {

        return campusNumber;
    }

    public void setCampusNumber(Integer campusNumber) {
        this.campusNumber = campusNumber;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getConstructionYear() {
        return constructionYear;
    }

    public void setConstructionYear(Integer constructionYear) {
        this.constructionYear = constructionYear;
    }

    public Integer getAreaSqm() {
        return areaSqm;
    }

    public void setAreaSqm(Integer areaSqm) {
        this.areaSqm = areaSqm;
    }

    public Integer getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(Integer numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public String getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String administrator) {
        this.administrator = administrator;
    }

    public String getComitteChairman() {
        return comitteChairman;
    }

    public void setComitteChairman(String comitteChairman) {
        this.comitteChairman = comitteChairman;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
