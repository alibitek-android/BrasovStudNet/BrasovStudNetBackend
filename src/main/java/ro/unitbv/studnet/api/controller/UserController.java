package ro.unitbv.studnet.api.controller;

import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ro.unitbv.studnet.api.domain.*;
import ro.unitbv.studnet.api.service.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/1/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private static Logger logger = org.slf4j.LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    // http://localhost:8000/api/1/users/students/topic?topicAddress=380247e2713ea05e9bc001ebe20c2bb498cd476b
    // http://192.168.1.5:8000/api/1/students/topic?topicAddress=754d4dac04735469c6447fe7f6bec11d9db28f54
    @RequestMapping(value = "/students/topic", method = RequestMethod.GET)
    public List<Student> getStudentsForTopic(@RequestParam(value = "topicAddress") String topicAddress)
    {
        logger.info("URL: {} Topic: {}", "/api/1/users/students/topic", topicAddress);
        return userService.getStudentsForTopic(topicAddress);
    }

    // GET /users/profile
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public User getCurrentUserDetails(Principal user)
    {
        return userService.getUserDetails(user.getName());
    }

    // GET /users/:id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User getUserById(@PathVariable String id)
    {
        return userService.getUserById(id);
    }

    // PUT /users/:id
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateUserById(@PathVariable String id, @RequestBody User user)
    {

    }

    // DELETE /users/:id
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteUserById(@PathVariable String id)
    {

    }

    // GET /users/students
    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public List<Student> getAllStudents()
    {
        return userService.getAllStudents();
    }

    // GET /users/teachers
    @RequestMapping(value = "/teachers", method = RequestMethod.GET)
    public List<Student> getAllTeachers()
    {
        return userService.getAllStudents();
    }

    // GET /users/users
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<Student> getAllUsers()
    {
        return userService.getAllStudents();
    }

//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
//    public ResponseObject handleException(Exception e) {
//        logger.error("{}", e.getMessage());
//        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage());
//    }
}
