package ro.unitbv.studnet.api.domain;

import java.io.Serializable;

public class Token implements Serializable {
    private String value;

    public Token() {

    }

    public Token(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
