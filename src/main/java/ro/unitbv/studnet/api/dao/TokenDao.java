package ro.unitbv.studnet.api.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import ro.unitbv.studnet.api.domain.Token;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.Map;

@Repository
public class TokenDao extends JdbcDaoSupport implements ITokenDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    protected void init() {
        setDataSource(dataSource);
    }

    //private static final String REGISTER_TOKEN_QUERY = "insert into user_tokens (user_id, token) values ((select u.id from users u join student_enrollments e on u.user_id = e.id join roles r on u.role_id = r.id and r.name = 'student' where email = :email), :token)";

    @Override
    public boolean registerToken(Token token, String email) {
        logger.info("Registering token: {} for user with email: {}", token, email);

        SqlParameterSource params = new MapSqlParameterSource().addValue("pemail", email).addValue("ptoken", token);

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getJdbcTemplate())
                .withSchemaName("api")
                .withProcedureName("upsert_token")
                .useInParameterNames("ptoken", "pemail")
                .withReturnValue();

        jdbcCall.addDeclaredParameter(new SqlParameter("ptoken", Types.VARCHAR));
        jdbcCall.addDeclaredParameter(new SqlParameter("pemail", Types.VARCHAR));

        Map<String, Object> out = jdbcCall.execute(params);

        if (out != null) {
            logger.debug(out.toString());
//
//            for (Map.Entry<String, Object> entry : out.entrySet()) {
//                logger.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//            }

            return (boolean) out.get("returnvalue");
        } else {
            return false;
        }
    }
}
