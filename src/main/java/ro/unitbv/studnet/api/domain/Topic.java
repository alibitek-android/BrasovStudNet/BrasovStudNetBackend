package ro.unitbv.studnet.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Topic {
    private String name;
    private String address;
    private TopicType type;

    enum TopicType {
        SCHOOL,
        CAMPUS,
        BUSINESS
    }

    public Topic() {
    }

    public Topic(String name, String address, TopicType type) {
        this.name = name;
        this.address = address;
        this.type = type;
    }

    public Topic(String name, String address, String type) {
        this.name = name;
        this.address = address;
        setType(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public TopicType getType() {
        return type;
    }

    public void setType(TopicType type) {
        this.type = type;
    }
    public void setType(String type) {
        this.type = TopicType.valueOf(type.toUpperCase());
    }

}
