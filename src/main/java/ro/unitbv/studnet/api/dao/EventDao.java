package ro.unitbv.studnet.api.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;
import ro.unitbv.studnet.api.domain.Event;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class EventDao extends JdbcDaoSupport implements IEventDao {

    private static final Logger logger = LoggerFactory.getLogger(EventDao.class);

    @Autowired
    private DataSource dataSource;

    private static final String ALL_EVENTS_QUERY = "select name, description, start_at, end_at from events_users us join events e on e.id = us.event_id join users u on u.id = us.user_id where u.id = api.get_user_id(?)";
    private static final String EVENTS_AT_QUERY = "select name, description, start_at, end_at from events_users us join events e on e.id = us.event_id join users u on u.id = us.user_id where u.id = api.get_user_id(?) and date_part('year', start_at) = ? and date_part('month', start_at) = ?";

    @PostConstruct
    protected void init() {
        setDataSource(dataSource);
    }

    private final class EventMapper implements RowMapper<Event> {

        @Override
        public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Event(rs.getString("name"), rs.getString("description"), rs.getTimestamp("start_at"), rs.getTimestamp("end_at"));
        }
    }

    @Override
    public List<Event> getEvents() {
        String email = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return getJdbcTemplate().query(ALL_EVENTS_QUERY, new Object[] {email}, new EventMapper());
    }

    @Override
    public List<Event> getEventsByYearMonth(int year, int month) {
        String email = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return getJdbcTemplate().query(EVENTS_AT_QUERY, new Object[] {email, year, month}, new EventMapper());
    }
}
