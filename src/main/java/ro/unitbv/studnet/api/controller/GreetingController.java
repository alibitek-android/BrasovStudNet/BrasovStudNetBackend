package ro.unitbv.studnet.api.controller;

import ro.unitbv.studnet.api.domain.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/1")
public class GreetingController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public Greeting greeting(Principal principal, @RequestParam(value="name", required = false) String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name != null ? name : principal.getName()));
    }

    @RequestMapping(value = "/greeting3", method = RequestMethod.GET)
    public Greeting greeting2(Principal principal, @RequestParam(value="name", required = false) String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name != null ? name : principal.getName()));
    }
}
