package ro.unitbv.studnet.api.service;

import ro.unitbv.studnet.api.domain.Role;
import ro.unitbv.studnet.api.domain.Student;
import ro.unitbv.studnet.api.domain.User;

import java.util.List;

public interface IUserService {
    User getUserDetails(String email);

    List<Student> getAllStudents();

    List<Student> getStudentsForTopic(String topicAddress);

    User getUserById(String id);
}
