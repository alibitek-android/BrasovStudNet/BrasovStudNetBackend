package ro.unitbv.studnet.api.service;

import ro.unitbv.studnet.api.domain.Event;

import java.util.List;

public interface IEventService {
    List<Event> getEvents();

    List<Event> getEventsByYearMonth(int year, int month);
}
