package ro.unitbv.studnet.api.dao;

import ro.unitbv.studnet.api.domain.Event;

import java.util.List;

public interface IEventDao {
    List<Event> getEvents();

    List<Event>getEventsByYearMonth(int year, int month);
}
