package ro.unitbv.studnet.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.unitbv.studnet.api.dao.IUserDao;
import ro.unitbv.studnet.api.domain.Student;
import ro.unitbv.studnet.api.domain.User;

import java.util.List;

@Component
public class UserService implements IUserService {

    @Autowired
    IUserDao userDao;

    @Override
    public User getUserDetails(String email) {
        return userDao.getUserDetails(email);
    }

    @Override
    public List<Student> getAllStudents() {
        return userDao.getAllStudents();
    }

    @Override
    public List<Student> getStudentsForTopic(String topicAddress) {
        return userDao.getStudentsForTopic(topicAddress);
    }

    @Override
    public User getUserById(String id) {
        return userDao.getUserById(id);
    }

}
