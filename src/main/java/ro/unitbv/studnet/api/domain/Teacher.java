package ro.unitbv.studnet.api.domain;

import java.util.List;

public class Teacher extends User {
    private String jobTitle;
    private String leadershipTitle;

    public Teacher() {
        super();
    }

    public Teacher(String name, String email, String role) {
        super(name, email, role, null);
    }

    public Teacher(String name, String email, String role, List<Topic> topics) {
        super(name, email, role, topics);
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLeadershipTitle() {
        return leadershipTitle;
    }

    public void setLeadershipTitle(String leadershipTitle) {
        this.leadershipTitle = leadershipTitle;
    }

    public Teacher(String name, String email, String role, List<Topic> topics, String jobTitle, String leadershipTitle) {

        super(name, email, role, topics);
        this.jobTitle = jobTitle;
        this.leadershipTitle = leadershipTitle;
    }
}
