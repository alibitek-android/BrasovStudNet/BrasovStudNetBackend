package ro.unitbv.studnet.api.domain;

import java.util.List;

public class Student extends User {
    private Enrollment enrollment;
    private Accomodation accomodation;
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Student() {
        super();
    }

    public Student(String name) {
        super(name);
    }

    public Student(String name, String email, String role) {
        super(name, email, role, null);
    }

    public Student(String id, String name, String email, String role) {
        super(id, name, email, role, null);
    }

    public Student(String name, String email, String role, List<Topic> topics) {
        super(name, email, role, topics);
    }

    public Student(String id, String name, String email, String role, List<Topic> topics) {
        super(id, name, email, role, topics);
    }

    public Student(String id, String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation) {
        super(id, name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
    }

    public Student(String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation) {
        super(name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
    }

    public Student(String name, String email, String role, List<Topic> topics, Enrollment enrollment, Accomodation accomodation, Location location) {
        super(name, email, role, topics);
        this.enrollment = enrollment;
        this.accomodation = accomodation;
        this.location = location;
    }

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }

    public Accomodation getAccomodation() {
        return accomodation;
    }

    public void setAccomodation(Accomodation campus) {
        this.accomodation = campus;
    }
}
