package ro.unitbv.studnet.api.dao;

import ro.unitbv.studnet.api.domain.Token;

public interface ITokenDao {
    boolean registerToken(Token token, String email);
}
