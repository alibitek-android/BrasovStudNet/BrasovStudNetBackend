package ro.unitbv.studnet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ro.unitbv.studnet.api.domain.Token;
import ro.unitbv.studnet.api.service.ITokenService;
import ro.unitbv.studnet.api.service.UserService;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/1/tokens", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class TokenController {

    @Autowired
    private ITokenService tokenService;

    @RequestMapping(value = { "", "/" }, method = RequestMethod.POST)
    public void registerToken(Principal principal, @RequestBody Token token) {
        tokenService.register(token, principal.getName());
    }

    @RequestMapping(value = "/unregister", method = RequestMethod.POST)
    public void unregisterToken(Principal principal, @RequestParam(value = "token") String token) {

    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void getTokensForUser(Principal principal) {
    }
}
