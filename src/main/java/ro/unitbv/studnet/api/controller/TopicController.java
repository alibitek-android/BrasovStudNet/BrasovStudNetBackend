package ro.unitbv.studnet.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.unitbv.studnet.api.domain.Student;
import ro.unitbv.studnet.api.domain.Topic;
import ro.unitbv.studnet.api.service.ITokenService;
import ro.unitbv.studnet.api.service.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/1/topics")
public class TopicController {

    private static final Logger logger = LoggerFactory.getLogger(TopicController.class);

    @RequestMapping(value = { "", "/"}, method = RequestMethod.GET)
    public List<Topic> getAllTopics()
    {
        return null;
    }

    @RequestMapping(value = { "/announceSubscribed"}, method = RequestMethod.POST)
    public void announceSubscribed(Principal user, List<Topic> topics)
    {
        logger.info("Announcing subscription to #{} topics by: {}", String.valueOf(topics.size()), user.getName());
    }


}
