package ro.unitbv.studnet.api.dao;

import ro.unitbv.studnet.api.domain.Role;
import ro.unitbv.studnet.api.domain.Student;
import ro.unitbv.studnet.api.domain.Topic;
import ro.unitbv.studnet.api.domain.User;

import java.util.List;

public interface IUserDao {
    User getUserDetails(String email);
    List<Student> getStudentsForTopic(String topicAddress);
    List<Student> getAllStudents();

    User getUserById(String id);
}
